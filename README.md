### When starting the Spring Boot application

2019-04-07 18:07:21.820  INFO 18116 --- [ntainer#2-0-C-1] o.a.k.c.c.internals.AbstractCoordinator  : [Consumer clientId=json-0, groupId=my-group] Successfully joined group with generation 3
2019-04-07 18:07:21.820  INFO 18116 --- [ntainer#0-0-C-1] o.a.k.c.c.internals.AbstractCoordinator  : [Consumer clientId=bytearray-0, groupId=my-group] Successfully joined group with generation 3
2019-04-07 18:07:21.820  INFO 18116 --- [ntainer#1-0-C-1] o.a.k.c.c.internals.AbstractCoordinator  : [Consumer clientId=string-0, groupId=my-group] Successfully joined group with generation 3
2019-04-07 18:07:21.822  INFO 18116 --- [ntainer#2-0-C-1] o.a.k.c.c.internals.ConsumerCoordinator  : [Consumer clientId=json-0, groupId=my-group] Setting newly assigned partitions [quote-topic-1]
2019-04-07 18:07:21.822  INFO 18116 --- [ntainer#0-0-C-1] o.a.k.c.c.internals.ConsumerCoordinator  : [Consumer clientId=bytearray-0, groupId=my-group] Setting newly assigned partitions [quote-topic-0]
2019-04-07 18:07:21.822  INFO 18116 --- [ntainer#1-0-C-1] o.a.k.c.c.internals.ConsumerCoordinator  : [Consumer clientId=string-0, groupId=my-group] Setting newly assigned partitions [quote-topic-2]
2019-04-07 18:07:21.832  INFO 18116 --- [ntainer#2-0-C-1] o.s.k.l.KafkaMessageListenerContainer    : partitions assigned: [quote-topic-1]
2019-04-07 18:07:21.832  INFO 18116 --- [ntainer#1-0-C-1] o.s.k.l.KafkaMessageListenerContainer    : partitions assigned: [quote-topic-2]
2019-04-07 18:07:21.832  INFO 18116 --- [ntainer#0-0-C-1] o.s.k.l.KafkaMessageListenerContainer    : partitions assigned: [quote-topic-0]

###
Kafka messages with the same key are always placed in the same partitions !! When we request /quote-of-the-day multiple times we see that the same key are always placed in the same partition.

###
