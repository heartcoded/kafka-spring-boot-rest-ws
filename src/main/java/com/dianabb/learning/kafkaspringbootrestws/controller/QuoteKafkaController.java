package com.dianabb.learning.kafkaspringbootrestws.controller;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.header.Headers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dianabb.learning.kafkaspringbootrestws.model.Quote;

@RestController
public class QuoteKafkaController {

	private static final Logger LOGGER = LoggerFactory.getLogger(QuoteKafkaController.class);

	private final KafkaTemplate<String, Object> template;
	private final String topicName;
	private final int messagesPerRequest;
	// the lock idea is not a pattern to use in a real application
	private CountDownLatch latch;

	public QuoteKafkaController(final KafkaTemplate<String, Object> template,
			@Value("${tpd.topic-name}") final String topicName,
			@Value("${tpd.messages-per-request}") final int messagesPerRequest) {
		this.template = template;
		this.topicName = topicName;
		this.messagesPerRequest = messagesPerRequest;
	}

	@GetMapping("/quote-of-the-day")
	public String hello() throws Exception {
		latch = new CountDownLatch(messagesPerRequest);
		IntStream.range(0, messagesPerRequest).forEach(i -> this.template.send(topicName, String.valueOf(i),
				new Quote("Your most unhappy customers are your greatest source of learning.— Bill Gates", i)));
		latch.await(60, TimeUnit.SECONDS);
		LOGGER.info("All messages received. Yey!");
		return "Sending quote of the day to Kafka...";
	}

	// Configure Listeners

	@KafkaListener(topics = "${tpd.topic-name}", clientIdPrefix = "json", containerFactory = "kafkaListenerContainerFactory")
	public void listenAsObject(ConsumerRecord<String, Quote> cr, @Payload Quote payload) {
		LOGGER.info("Logger 1 [JSON] received key {}: Type [{}] | Payload: {} | Record: {}", cr.key(),
				typeIdHeader(cr.headers()), payload, cr.toString());
		latch.countDown();
	}

	@KafkaListener(topics = "${tpd.topic-name}", clientIdPrefix = "string", containerFactory = "kafkaListenerStringContainerFactory")
	public void listenasString(ConsumerRecord<String, String> cr, @Payload String payload) {
		LOGGER.info("Logger 2 [String] received key {}: Type [{}] | Payload: {} | Record: {}", cr.key(),
				typeIdHeader(cr.headers()), payload, cr.toString());
		latch.countDown();
	}

	@KafkaListener(topics = "${tpd.topic-name}", clientIdPrefix = "bytearray", containerFactory = "kafkaListenerByteArrayContainerFactory")
	public void listenAsByteArray(ConsumerRecord<String, byte[]> cr, @Payload byte[] payload) {
		LOGGER.info("Logger 3 [ByteArray] received key {}: Type [{}] | Payload: {} | Record: {}", cr.key(),
				typeIdHeader(cr.headers()), payload, cr.toString());
		latch.countDown();
	}

	private static String typeIdHeader(Headers headers) {
		return StreamSupport.stream(headers.spliterator(), false).filter(header -> header.key().equals("__TypeId__"))
				.findFirst().map(header -> new String(header.value())).orElse("N/A");
	}

}
