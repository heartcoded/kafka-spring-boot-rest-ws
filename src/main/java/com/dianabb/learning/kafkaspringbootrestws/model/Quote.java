package com.dianabb.learning.kafkaspringbootrestws.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Quote {

	private final String quote;
	private final int identifier;

	public Quote(@JsonProperty("quote") final String quote, @JsonProperty("identifier") final int identifier) {
		this.quote = quote;
		this.identifier = identifier;
	}

	public String getQuote() {
		return quote;
	}

	public int getIdentifier() {
		return identifier;
	}

	@Override
	public String toString() {
		return "Quote [quote=" + quote + ", identifier=" + identifier + "]";
	}

}
